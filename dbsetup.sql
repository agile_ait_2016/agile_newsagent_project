DROP DATABASE IF EXISTS newspaper;

CREATE DATABASE newspaper;

USE newspaper;

CREATE TABLE customer(
  id int AUTO_INCREMENT,
  name VARCHAR(100),
  surname VARCHAR(100),
  address1 VARCHAR(100),
  address2 VARCHAR(100),
  zoneId int,
  telNumber VARCHAR(100),
  email VARCHAR(100),
  PRIMARY KEY (id)
);
CREATE TABLE exCustomer(
  id int AUTO_INCREMENT,
  name VARCHAR(100),
  surname VARCHAR(100),
  address1 VARCHAR(100),
  address2 VARCHAR(100),
  zoneId int,
  telNumber VARCHAR(100),
  email VARCHAR(100),
  PRIMARY KEY (id)
);

CREATE TABLE bill(
  id int AUTO_INCREMENT,
  billDate DATE,
  amount float,
  description VARCHAR(100),
  customerId int,
  PRIMARY KEY (id),
  FOREIGN KEY (customerId) REFERENCES customer(id)
);

CREATE TABLE media(
  id int AUTO_INCREMENT,
  contentType VARCHAR(100),
  name VARCHAR(100),
  language VARCHAR(100),
  PRIMARY KEY (id)
);

CREATE TABLE subscription(
  customerId int,
  mediaId int,
  type VARCHAR(100),
  PRIMARY KEY (customerId,mediaId),
  FOREIGN KEY (customerId) REFERENCES customer(id),
  FOREIGN KEY (mediaId) REFERENCES media(id)
);

CREATE TABLE holidays(
  customerId int,
  fromDate date,
  toDate date,
  holiday boolean,
  FOREIGN KEY (customerId) REFERENCES customer(id)
);

CREATE TABLE user(
  id int AUTO_INCREMENT,
  username VARCHAR(100),
  password VARCHAR(100),
  password1 VARCHAR(100),
  userType int,
  PRIMARY KEY (id)
);

INSERT INTO media VALUES (null,"newspaper","irish times","irish");
INSERT INTO media VALUES (null,"newspaper","irish times","english");
INSERT INTO media VALUES (null,"magazine","play boy","english");
INSERT INTO media VALUES (null,"magazine","cars","english");

INSERT INTO user VALUES (null,"ShopOwener","Administrator","Administrator",null);
INSERT INTO user VALUES (null,"deliveryboy","Dboy123455","Dboy123455",null);

INSERT INTO customer VALUES (null,"jorge","manzanares","croi oige","apt 13",null,"698563254","jorgemanza@gmail.com");
INSERT INTO customer VALUES (null,"nico","marced","croi oige","apt 2",null,"698566254","nicomarced@gmail.com");

INSERT INTO subscription VALUES (1,1,"Daily");
INSERT INTO subscription VALUES (2,1,"Monthly");

INSERT INTO holidays VALUES (1,"2011-10-02","2016-11-28",false);
INSERT INTO holidays VALUES (2,"2015-02-02","2015-10-9",false);