package agile_newsagent_project;

import java.text.ParseException;
import java.util.Date;

public class Holiday {
	private int customerId;
	private Date date = new Date();
	private Date from = null;
	private Date to = null;
	boolean newspaper;
	boolean holiday = false;

	public Holiday() {
	}

	public Holiday(Date from, Date to) {
		this.from = from;
		this.to = to;
	}

	public Date getFrom() {
		return from;
	}

	public void setid(int id) {
		customerId = id;
	}

	public Date getTo() {
		return to;
	}
	public void setHoliday(boolean holiday){
		this.holiday = holiday;
		DB connector = new DB();
		connector.setHoliday(customerId,holiday);
	}

	public void setDate(Date fromDate, Date toDate,boolean holiday) {
		from = fromDate;
		to = toDate;
		this.holiday = holiday;
	}

	public void setHoliday(Date from, Date to,boolean holiday) {
		DB connector = new DB();
		connector.setHoliday(customerId, from, to, holiday);
	}

	public boolean getHoliday() throws ParseException {
		if (holiday == true) {
			DB connector = new DB();
			connector.getHoliday(customerId);
			return !(date.before(from) && date.after(to));
		} else
			return false;
	}

	public boolean getNewspaper() throws ParseException {
		
		if (holiday == true) {
			DB connector = new DB();
			connector.getHoliday(customerId);
			return (date.before(from) && date.after(to));
		}
		 else 
			return true;
	}
}
