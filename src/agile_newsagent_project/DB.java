package agile_newsagent_project;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DB {

	private Connection con;
	private Statement stmt;
	private ResultSet rs = null;

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/newspaper";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}

	public void addDetails() {

	}

	public void registerValidation() {

	}

	public boolean registerValidation(String name, String password, String password1, int userType) {
		initiate_db_conn();
		boolean register = false;
		try {
			String updateTemp = "INSERT INTO user VALUES(null,'" + name + "','" + password + "','" + password1 + "',"
					+ userType + ");";
			stmt.executeUpdate(updateTemp);
			System.out.println(updateTemp);
			stmt.close();
			con.close();
			register = true;
		} catch (SQLException e) {
			register = false;
		}
		return register;
	}

	public boolean addDetails(String name, String surname, String address1, String address2, int zoneId,
			String telNumber, String email) {
		initiate_db_conn();
		boolean details = false;
		try {
			String updateTemp = "INSERT INTO customer VALUES(null,'" + name + "','" + surname + "','" + address1 + "','"
					+ address2 + "'," + zoneId + ",'" + telNumber + "','" + email + "');";
			stmt.executeUpdate(updateTemp);
			System.out.println(updateTemp);
			stmt.close();
			con.close();
			details = true;
		} catch (SQLException e) {
			details = false;
		}
		return details;
	}

	public boolean removeCustomer(int customerId) {
		initiate_db_conn();
		boolean delete = false;
		try {
			String update = "INSERT INTO exCustomer SELECT * FROM customer WHERE id= ( " + customerId + ");";
			String updateTemp = "DELETE FROM customer WHERE id=( " + customerId + ");";
			stmt.executeUpdate(update);
			stmt.executeUpdate(updateTemp);
			System.out.println(update);
			System.out.println(updateTemp);
			stmt.close();
			con.close();
			delete = true;

		} catch (SQLException e) {
			delete = false;
		}

		return delete;

	}

	public boolean removeCustomer(String name, String surname) {
		initiate_db_conn();
		boolean delete = false;
		try {
			String update = "INSERT INTO exCustomer SELECT * FROM customer WHERE name= '" + name + "'AND surname = '"
					+ surname + "';";
			String updateTemp = "DELETE FROM customer WHERE name= '" + name + "'AND surname = '" + surname + "';";
			stmt.executeUpdate(update);
			stmt.executeUpdate(updateTemp);
			System.out.println(updateTemp);
			stmt.close();
			con.close();
			delete = true;
		} catch (SQLException e) {
			delete = false;
		}
		return delete;

	}
	
	public String getAddress(int customerId) {
		initiate_db_conn();
		
		String address1 = null;
		String address2 = null;
		try {
			String getAddress = "SELECT address1,address2 FROM customer WHERE id = "+customerId+";";
			ResultSet rs = stmt.executeQuery(getAddress);
			if(!rs.next()){
				return null;
			}
			address1 =rs.getString("address1");
			address2 =rs.getString("address2");

			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			System.out.print("error on getAddress");
			
		}
		return address1.concat(address2);
	}
	
	public void setSubscription(int customerId,String type) {
		initiate_db_conn();
		try {
			String updateType = "UPDATE subscription " +
                   "SET type = '"+type+"' WHERE customerId = "+customerId+";";
			stmt.executeUpdate(updateType);
			
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error in setSubscription");
		}
	}
	public void   deleteSubscription(int customerId){
		initiate_db_conn();
		try {
			String deleteSub = "DELETE * FROM subscription WHERE customerId = "+customerId+";";
			ResultSet rs = stmt.executeQuery(deleteSub);
			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			System.out.println("error on getSubscription");
		}
	}
	public String getSubscription(int customerId) {
		initiate_db_conn();
		String type = null;
		try {
			String getType = "SELECT type FROM subscription WHERE customerId = "+customerId+";";
			ResultSet rs = stmt.executeQuery(getType);

			if(rs.next()){
				type =rs.getString("type");

			}
			else{
				type = "no data";
			}
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("error on getSubscription");
		}
		return type;
	}

		
	public void cancelBill(int billId){
		initiate_db_conn();
		try {
			String deleteBill = "DELETE * FROM bill WHERE id = "+billId+";";
			ResultSet rs = stmt.executeQuery(deleteBill);
			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			System.out.println("error on deleteBill");
		}
		
		
	}
	
	public void setHoliday(int customerId,Date from,Date to,boolean holiday) {
		initiate_db_conn();

		String From = new SimpleDateFormat("yyyy-MM-dd").format(from);
		String To = new SimpleDateFormat("yyyy-MM-dd").format(to);

		try {
			String updateHoliday = "UPDATE holidays " +
                   "SET fromDate = '"+From+"', toDate = '"+To+"', holiday = "+holiday+" WHERE customerId = "+customerId+";";
			
			stmt.executeUpdate(updateHoliday);
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error in setHoliday");
		}
	}
	public void setHoliday(int customerId,boolean holiday) {
		initiate_db_conn();


		try {
			String updateHoliday = "UPDATE holidays " +
                   "SET holiday = "+holiday+" WHERE customerId = "+customerId+";";
			
			stmt.executeUpdate(updateHoliday);
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error in setHoliday(id,bool)");
		}
	}
	
	public void getHoliday(int customerId) throws ParseException {
		initiate_db_conn();
		Holiday holiday = new Holiday();
		String from = null;
		String to = null;
		boolean onHoliday = false;
		
		try {
			String getType = "SELECT fromDate,toDate,holiday FROM holidays WHERE customerId = "+customerId+";";
			ResultSet rs = stmt.executeQuery(getType);
			while(rs.next()){
				from =rs.getString("fromDate");
				to =rs.getString("toDate");
				onHoliday =rs.getBoolean("holiday");
			}
			Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(from);
			Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(to);
			
		holiday.setDate(fromDate, toDate, onHoliday);
		stmt.close();
		con.close();
			
		} catch (SQLException e) {
			System.out.println("error on getHoliday");
		}
	}
	
	public boolean addBill(Date date, float amount, String description){
		initiate_db_conn();
		boolean register = false;
		try {
			String updateTemp = "INSERT INTO bill (billDate,amount,description) VALUES ("+date+","+amount+","
							+description+");";
			stmt.executeUpdate(updateTemp);
			System.out.println(updateTemp);
			stmt.close();
			con.close();
			register = true;
		} catch (SQLException e) {
			e.printStackTrace();
			register = false;
		}
		return register;
	}

	public boolean loginUser(String username, String password, int userType) throws SQLException
	{
		initiate_db_conn();
		boolean login = false;

		try
		{
			rs = stmt.executeQuery("SELECT * FROM USER Where userType = " + userType + " AND username = '" + username + "'" + "AND password = '" + password + "';");
			System.out.println(rs);
			if (rs.next())
			{
				login = true;
				System.out.println(rs);
			}

			stmt.close();
			con.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
			login = false;
		}

		if (login == true)
		{
			System.out.println("Successfully logged in....");
		}
		return login;

	}

	public boolean updateCustomer(String firstName, String lastName, String address)
	{
		initiate_db_conn();
		boolean update = false;
		try
		{
			String updateCus = "UPDATE customer " + "SET address1 = '"+address+"' WHERE name = '"+firstName+"' AND surname= '"+lastName+"';";
					//"UPDATE customer SET " + "address1 = '" + address + "' where name = "+ firstName + " AND surname = '" + lastName + "';";

			stmt.executeUpdate(updateCus);
			rs = stmt.executeQuery("SELECT * from customer ");
			rs.next();
			rs.close();
			update = true;
		}
		catch (SQLException sqle)
		{
			System.err.println("Error with  update:\n" + sqle.toString());
			update = false;
		}
		return update;
	}

	public boolean updateSubscription(int cusId, int mediaId, String subType)
	{
		initiate_db_conn();
		boolean update = false;
		
		try
		{
			String updateSub = "UPDATE subscription SET " + "type = '" + subType + "'  where customerId = "+ cusId;

			stmt.executeUpdate(updateSub);
			rs = stmt.executeQuery("SELECT * from subscription ");
			rs.next();
			rs.close();
			update = true;
		}
		catch (SQLException sqle)
		{
			System.err.println("Error with  update:\n" + sqle.toString());
			update = false;
		}
		return update;
	}
	

}
