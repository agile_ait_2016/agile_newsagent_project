
package agile_newsagent_project;

import java.util.ArrayList;
import java.util.List;

public class Subscription {

	String type;
	int setType;
	List<Media> media;
	int preferedDay;
	int customerID;
	int mediaID;
	int subcriptionID;
	
	public Subscription(String type){
		this.type = type;
		media = new ArrayList<Media>();
	}
	
	public Subscription()
	{
		
	}

	public void addMedia(Media media){
		this.media.add(media);
	}
	
	public void removeMedia(Media media){
	
	}
    public String displaySubscription(int customerId){
    	DB connector = new DB();
    	return connector.getSubscription(customerID);
    	
    }
	public int getPreferedDay() {
		return preferedDay;
	}

	public void setPreferedDay(int preferedDay) {
		this.preferedDay = 1;
		if(type == "Monthly"){
			if(preferedDay>0 && preferedDay<31)
				this.preferedDay = preferedDay;
		}else if(type == "Weekly"){
			if(preferedDay>0 && preferedDay<8)
				this.preferedDay = preferedDay;
		}
	}
	public void cancelSubscription(int customerId){
		DB connector = new DB();
		connector.deleteSubscription(customerId);
		
	}
	
	
	public boolean updateSubscription(int cusId, int mediaId, String subType) throws RegisterExceptionHandler
	{
		boolean updateSub = true;
		
		if (cusId < 0)
		{
			updateSub = false;
			throw new RegisterExceptionHandler("There is no subscription");
		}
		else if (updateSub)
		{
			DB db = new DB();
			db.updateSubscription(cusId, mediaId, subType);
			
			System.out.println("The subscription for customer number: "+ cusId + " has been changed" );
		}
		
		return updateSub;
	}
	
	public void setType(int setType){//type of subscription
		DB connector = new DB();
		if(setType ==0){
			type = "Non-Subscriber";
		}
		else if(setType ==1){
			type = "Daily";
		}
		else if(setType ==2){
			type = "Weekly";
		}
		else if(setType ==3){
			type = "Monthly";
		}
		else{
			type = "Error";
		}
		connector.setSubscription(customerID,type);
	}
	
	public String getType(){
		DB connector = new DB();
		return connector.getSubscription(customerID);
	}
	public void setCustomerID(int ID){
		customerID=ID;
	}
	public int getCustomerID()
	{
		return customerID;
	}
	public void setmediaID(int ID){
		    mediaID=ID;
	}
	public int getmediaID()
	{
		return mediaID;
	}
	public void setsubscriptionID(int ID){
	    mediaID=ID;
    }
    public int getsubscriptionID()
   {
	return mediaID;
    }
}