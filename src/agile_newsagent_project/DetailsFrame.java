package agile_newsagent_project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class DetailsFrame extends JFrame implements ActionListener {
	AddCustomer customer = new AddCustomer();
	Bill bill = new Bill();
	private JTextField tfFirstName = new JTextField();
	private JTextField tfLastName = new JTextField();
	private JTextField tfAddOne = new JTextField();
	private JTextField tfAddTwo = new JTextField();
	private JTextField tfZone = new JTextField();
	private JTextField tfTelephone = new JTextField();
	private JTextField tfEmail = new JTextField();
	private JTextField tfDeleteSN = new JTextField();
	private JTextField tfDeleteId = new JTextField();

	private JTextField tfDate = new JTextField();
	private JTextField tfAmount = new JTextField();
	private String[] options = {"No selected","Cash", "Card", "PayCheck"};
	private JComboBox optionList = new JComboBox(options);
	private JTextField tfDescription = new JTextField();
	
	private JTextArea textarea = new JTextArea("", 6, 8);
	
	private JButton btnCreateBill = new JButton("Create bill");
	private JButton btnPrintBill = new JButton("Print bill");
	private JButton btnRegisterCus = new JButton("Register Customer");
	private JButton btnUpdateDetails = new JButton("Update Address");

	private JButton btnDelete = new JButton("Delete By ID!");
	private JTextField tfDeleteFN = new JTextField();
	private final JLabel lblFirstName = new JLabel("First Name:");
	private final JLabel lblSurname = new JLabel("Surname:");
	private final JButton btnDeleteByName = new JButton("Delete By Name");

	public DetailsFrame() {
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(
				new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0),
						new Color(0, 0, 0)),
				"Add Customer Details", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 11, 237, 266);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("First Name:");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 22, 90, 22);
		panel.add(lblNewLabel);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblLastName.setBounds(10, 46, 90, 22);
		panel.add(lblLastName);

		JLabel lblAddressLine = new JLabel("Address Line 1:");
		lblAddressLine.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAddressLine.setBounds(10, 69, 104, 22);
		panel.add(lblAddressLine);

		JLabel lblAddressLine_1 = new JLabel("Address Line 2:");
		lblAddressLine_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAddressLine_1.setBounds(10, 94, 104, 22);
		panel.add(lblAddressLine_1);

		JLabel lblZoneId = new JLabel("Zone ID:");
		lblZoneId.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblZoneId.setBounds(10, 119, 90, 22);
		panel.add(lblZoneId);

		JLabel lblTelephoneNo = new JLabel("Telephone No:");
		lblTelephoneNo.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblTelephoneNo.setBounds(10, 141, 90, 22);
		panel.add(lblTelephoneNo);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblEmail.setBounds(10, 165, 90, 22);
		panel.add(lblEmail);

		tfFirstName.setBounds(121, 22, 86, 20);
		panel.add(tfFirstName);
		tfFirstName.setColumns(10);

		tfLastName.setBounds(121, 46, 86, 20);
		panel.add(tfLastName);
		tfLastName.setColumns(10);

		tfAddOne.setBounds(121, 71, 86, 20);
		panel.add(tfAddOne);
		tfAddOne.setColumns(10);

		tfAddTwo.setBounds(121, 96, 86, 20);
		panel.add(tfAddTwo);
		tfAddTwo.setColumns(10);

		tfZone.setBounds(121, 119, 86, 20);
		panel.add(tfZone);
		tfZone.setColumns(10);

		tfTelephone.setBounds(121, 143, 86, 20);
		panel.add(tfTelephone);
		tfTelephone.setColumns(10);
		tfEmail.setText("OPTIONAL");

		tfEmail.setBounds(121, 167, 86, 20);
		panel.add(tfEmail);
		tfEmail.setColumns(10);

		btnRegisterCus.setBounds(10, 198, 197, 23);
		panel.add(btnRegisterCus);

		btnUpdateDetails.setBounds(11, 232, 196, 23);
		panel.add(btnUpdateDetails);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(
				new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0),
						new Color(0, 0, 0)),
				"Remove Customer", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		panel_1.setBounds(10, 288, 237, 181);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel lblByName = new JLabel("Delete by Name:");
		lblByName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblByName.setBounds(51, 14, 145, 22);
		panel_1.add(lblByName);

		JLabel lblById = new JLabel("By ID:");
		lblById.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblById.setBounds(10, 95, 90, 22);
		panel_1.add(lblById);

		btnDelete.setBounds(10, 120, 196, 23);
		panel_1.add(btnDelete);

		tfDeleteSN.setBounds(120, 66, 86, 20);
		panel_1.add(tfDeleteSN);
		tfDeleteSN.setColumns(10);

		tfDeleteId.setBounds(120, 95, 86, 20);
		panel_1.add(tfDeleteId);
		tfDeleteId.setColumns(10);

		tfDeleteFN.setBounds(120, 41, 86, 19);
		panel_1.add(tfDeleteFN);
		tfDeleteFN.setColumns(10);
		lblFirstName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblFirstName.setBounds(10, 39, 90, 22);

		panel_1.add(lblFirstName);
		lblSurname.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblSurname.setBounds(10, 62, 90, 22);

		panel_1.add(lblSurname);
		btnDeleteByName.setBounds(10, 147, 196, 23);

		panel_1.add(btnDeleteByName);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(
				new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0),
						new Color(0, 0, 0)),
				"Customers Order", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		panel_2.setBounds(257, 11, 498, 458);
		getContentPane().add(panel_2);
		panel_2.setLayout(null);
		JLabel lblAmount = new JLabel("Amount:");
		JLabel lblDate = new JLabel("Date:");
		JLabel lblDesctiption = new JLabel("Description:");
		JLabel lblPayment = new JLabel("PaymentOption:");
		tfAmount.setColumns(10);
		tfDate.setColumns(10);
		tfDescription.setColumns(10);
		optionList.setSelectedIndex(0);
		textarea.setVisible(true);
		textarea.setText("");
		textarea.setBorder(BorderFactory.createLineBorder(Color.black));
		
		lblAmount.setBounds			(20,30, 100,23);
		lblDesctiption.setBounds	(20,65, 100,23);
		lblDate.setBounds			(20,100, 100,23);
		lblPayment.setBounds		(20,135, 100,23);
		tfAmount.setBounds			(140,30, 150,23);
		tfDescription.setBounds		(140,65, 150,23);
		tfDate.setBounds			(140,100, 150,23);
		optionList.setBounds		(140,135,150,23);
		btnCreateBill.setBounds		(330,40, 100,50);
		btnPrintBill.setBounds		(330,110,100,50);//left,top,width,height
		textarea.setBounds			(30,180,400, 90);
		panel_2.add(lblAmount);
		panel_2.add(tfAmount);
		panel_2.add(lblDate);
		panel_2.add(tfDate);
		panel_2.add(lblDesctiption);
		panel_2.add(tfDescription);
		panel_2.add(lblPayment);
		panel_2.add(optionList);
		panel_2.add(textarea);
		panel_2.add(btnCreateBill);
		panel_2.add(btnPrintBill);
		
		setSize(781, 518);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		btnCreateBill.addActionListener(this);
		btnPrintBill.addActionListener(this);
		btnRegisterCus.addActionListener(this);
		btnDelete.addActionListener(this);
		btnUpdateDetails.addActionListener(this);
		btnDeleteByName.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();

		if (target == btnRegisterCus) {

			try {
				boolean c = customer.addDetails(tfFirstName.getText(), tfLastName.getText(), tfAddOne.getText(),
						tfAddTwo.getText(), Integer.parseInt(tfZone.getText()), tfTelephone.getText(),
						tfEmail.getText());
				JOptionPane.showMessageDialog(null, "You have added a customer succesfully");
				tfFirstName.setText("");
				tfLastName.setText("");
				tfAddOne.setText("");
				tfAddTwo.setText("");
				tfZone.setText("");
				tfTelephone.setText("");
				tfEmail.setText("");

			} catch (RegisterExceptionHandler e1) {
				JOptionPane.showMessageDialog(null, "Exception:" + e1.getMessage());
				e1.printStackTrace();
			}
		}
		if (target == btnDelete) {

			try {
				boolean c = customer.removeCustomer(Integer.parseInt(tfDeleteId.getText()));
				JOptionPane.showMessageDialog(null,
						"You have deleted the customer succesfully\n Customer details are send to excustomer database table");
				tfDeleteId.setText("");
			} catch (RegisterExceptionHandler e1) {

				e1.printStackTrace();
			}

		}
		if (target == btnDeleteByName) {
			try {
				boolean c1 = customer.removeCustomer(tfDeleteFN.getText(), tfDeleteSN.getText());
				JOptionPane.showMessageDialog(null,
						"You have deleted the customer succesfully\n Customer details are send to excustomer database table");
				tfDeleteFN.setText("");
				tfDeleteSN.setText("");
			} catch (RegisterExceptionHandler e1) {

				e1.printStackTrace();
			}
		}

		if (target == btnUpdateDetails)
		{
			try
			{
				boolean ud = customer.updateCusDetails(tfFirstName.getText(), tfLastName.getText(), tfAddOne.getText());
				JOptionPane.showMessageDialog(null,"You have successfully updated address");
			}
			catch (RegisterExceptionHandler e1)
			{
				e1.printStackTrace();
			}
		}
		if(target==btnCreateBill){
			try {
				bill.setDate(tfDate.getText());
				bill.setAmount(Integer.parseInt(tfAmount.getText()));
				bill.setDescription(tfDescription.getText());
				if(optionList.getSelectedIndex() != 0)
					bill.setPaymentOption(optionList.getSelectedIndex()-1);
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (AmountHandler e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (DateExceptionHandler e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if(target == btnPrintBill){
			textarea.setText(bill.printAll());
		}
		

	}

}
