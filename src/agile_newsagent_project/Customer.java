package agile_newsagent_project;

import java.util.ArrayList;

public class Customer {
	private int customerId;
	private String name, surname, address1, address2, telNumber, email;
	private int zoneId;
	private ArrayList<Bill> bills;
	private ArrayList<Subscription> subscriptions;
	private ArrayList<Holiday> holidays;

	public Customer( String name, String surname, String address1, String address2, int zoneId,
			String telNumber, String email) {
	
	}
	public Customer(int customerId, String name, String surname, String address1, String address2, int zoneId,
			String telNumber, String email) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.surname = surname;
		this.address1 = address1;
		this.address2 = address2;
		this.zoneId = zoneId;
		this.telNumber = telNumber;
		this.email = email;
		this.bills = new ArrayList<Bill>();
		this.subscriptions = new ArrayList<Subscription>();
		this.holidays = new ArrayList<Holiday>();
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName(){
		if(name == null)
			return "No name entered";
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public void addBill(Bill bill){
		this.bills.add(bill);
	}
	public ArrayList<Bill> getBills() {
		return bills;
	}
	public void setBills(ArrayList<Bill> bills) {
		this.bills = bills;
	}

}
