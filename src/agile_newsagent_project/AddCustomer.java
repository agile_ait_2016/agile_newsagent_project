package agile_newsagent_project;

public class AddCustomer {
	public boolean addDetails(String name, String surname, String address1, String address2, int zoneId,
			String telNumber, String email) throws RegisterExceptionHandler {

		boolean details = true;

		if ((name.length() == 0) || (surname.length() == 0)) {
			details = false;
			throw new RegisterExceptionHandler("text field is empty must be have proper criteria entered, enter name");
		} else if ((address1.length() == 0) || (address2.length() == 0)) {
			details = false;
			throw new RegisterExceptionHandler(
					"text field is empty must be have proper criteria entered, enter address");
		} else if ((telNumber.length() == 0)) {
			details = false;
			throw new RegisterExceptionHandler(
					"text field is empty must be have proper criteria entered, enter phone no");
		} else if ((zoneId <= 0) || (zoneId > 4)) {
			details = false;
			throw new RegisterExceptionHandler("ZoneID must be a number from 1-4, enter number 1,2,3,4 for zoneID");
		} else if (details) {
			DB DB = new DB();
			DB.addDetails(name, surname, address1, address2, zoneId, telNumber, email);
			System.out.println("Details saved to Database");
		}

		return details;

	}

	public boolean removeCustomer(int customerId) throws RegisterExceptionHandler {

		boolean deleteByID = true;

		if ((customerId != customerId) || (customerId < 0)) {
			deleteByID = false;
			throw new RegisterExceptionHandler("Not a valid Id");
		}else if (deleteByID){
			DB DB = new DB();
			DB.removeCustomer(customerId);
			System.out.println("Customer deleted:"+ customerId);
		}

		return deleteByID;

	}

	public boolean removeCustomer(String name,String surname) throws RegisterExceptionHandler {

		boolean deleteByName = true;
		if ((!name.equals(name))|| (name.length()==0)&&(!surname.equals(name))|| (surname.length()==0)) {
			deleteByName = false;
			throw new RegisterExceptionHandler("Not a valid name");
		}else if (deleteByName){
			DB DB = new DB();
			DB.removeCustomer(name,surname);
			System.out.println("Customer deleted:"+ name+surname);
		}

		return deleteByName;

	}

	public boolean updateCusDetails(String firstName, String lastName, String address) throws RegisterExceptionHandler
	{
		boolean editByName = true;
		if ((firstName.length() <= 0) || (lastName.length() <= 0) )
		{
			editByName = false;
			throw new RegisterExceptionHandler("Invalid Name or surname");
		}
		else if (editByName){
			DB db = new DB();
			db.updateCustomer(firstName, lastName, address);
			System.out.println("The details for customer: "+ firstName + lastName + " have been changed" );
		}
		
		return editByName;
	}

}
