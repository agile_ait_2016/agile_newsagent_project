package agile_newsagent_project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class User {
	private int id;
	private int userType; //0 admin 1 deliveryBoy
	private String name, password,password1;
	private static UserDAO userDAO;

	public User( String password,String pasword1, String name, int userType) {	
	}

	public User(int id, String pasword1, String name, String password, int userType) {
		this.id = id;
		this.userType=userType;
		this.password = password;
		this.password1 = password1;
		this.name = name;
	}


	public static void setUserDAO(UserDAO userDAO){
		User.userDAO = userDAO;
	}

	public User() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String printDeliveries() throws Exception{
		if(name==null || name.equals(""))
			throw new Exception("The user name cannot be blank or null");
		String[] deliveries = userDAO.getDeliveries(name);
		String result = "User: "+name+"\nDeliveries:\n";
		
		if(deliveries.length==0){
			result += "No deliveries";
			return result;
		}

		for(String s:deliveries){
			String[] delivery = s.split("/");
			result += delivery[0] + " to " + delivery[1] + "("+delivery[2]+")\n";
		}

		return result;
	}

	public String[] getSortedDeliveries() throws Exception{
		if(name==null || name.equals(""))
			throw new Exception("The user name cannot be blank or null");
		ArrayList<String> deliveries = new ArrayList<String>(Arrays.asList(userDAO.getDeliveries(name)));
		
		Collections.sort(deliveries);

		return deliveries.toArray(new String[deliveries.size()]);
	}
}
