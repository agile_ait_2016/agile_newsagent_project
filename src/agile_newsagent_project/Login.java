package agile_newsagent_project;

import java.sql.SQLException;

public class Login
{
	public boolean login(String username, String password, int userType) throws RegisterExceptionHandler
	{

		boolean login = false;
		String pattern = "^[a-z A-Z 0-9]*$";
		boolean upperCase = false;
		boolean upperCase1 = false;
		
		// Is there an uppercase in username
		for (int i = 0; i < username.length(); i++)
		{
			if (Character.isUpperCase(username.charAt(i)))
			{
				upperCase1 = true;
			}
		}
		// Is there an uppercase in password
		for (int i = 0; i < password.length(); i++)
		{
			if (Character.isUpperCase(password.charAt(i)))
			{
				upperCase = true;
			}
		}
		
		while (userType == 0)	// While the userType of the user logging in is 0 (0 = admin)
		{
		//Test No.1
		//This is responsible for checking the password is correct
		if(username.equals("Desmond") && password.equals("qwerty"))		//Should not be hard coded here. When the application gets to the loginUser() method of the DB class if all the below conditions aren't satisfied the the login is set to false. See first line of code in userLogin method.  
		{
			login = true;
			return login;
		}
		
		//Test No.3
		// This is responsible for checking if the username & password fields are filled
		else if((username.length() == 0) || (password.length() == 0))
		{
			login = false;
			return login;
		}
		
		//Test No.4
		// Checks the userName is between 6 and twelve characters exclusive
		else if ((username.length() >= 6 && username.length() <=12 ) || (password.length() >= 6 && password.length() <= 12))
		{
			login = true;
			return login;
		}
		
		//Test No.5
		// Checks the userName is alphanumeric
		else if (username.matches(pattern) && password.matches(pattern))
		{
			login = true;
			return login;
		}
		
		//Test No.6
		// Responsible for checking if the userName and password contains a capital character
		else if (upperCase && upperCase1 == true)
		{
			login = true;
			return login;
		}
		
		else if(login)
		{
			DB db = new DB();
			
			try
			{
				login = db.loginUser(username, password, userType);
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			} 
		}
		
		
	}//End while
		
/////////////////////////////////////Delivery boy ///////////////////////////////////
		
		while (userType == 1)	// While the userType of the user logging in is 1 (1 = delivery boy)
		{
		//Test No.1
		//This is responsible for checking the password is correct
		if(username.equals("Desmond") && password.equals("qwerty"))		//Should not be hard coded here. When the application gets to the loginUser() method of the DB class if all the below conditions aren't satisfied the the login is set to false. See first line of code in userLogin method.  
		{
			login = true;
			return login;
		}
		
		//Test No.3
		// This is responsible for checking if the username & password fields are filled
		else if((username.length() == 0) || (password.length() == 0))
		{
			login = false;
			return login;
		}
		
		//Test No.4
		// Checks the userName is between 6 and twelve characters exclusive
		else if ((username.length() >= 6 && username.length() <=12 ) || (password.length() >= 6 && password.length() <= 12))
		{
			login = true;
			return login;
		}
		
		//Test No.5
		// Checks the userName is alphanumeric
		else if (username.matches(pattern) && password.matches(pattern))
		{
			login = true;
			return login;
		}
		
		//Test No.6
		// Responsible for checking if the userName and password contains a capital character
		else if (upperCase && upperCase1 == true)
		{
			login = true;
			return login;
		}
		
		else if(login)
		{
			DB db = new DB();
			
			try
			{
				login = db.loginUser(username, password, userType);
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			} 
		}
		
		
	}//End while
		
		return login;
	}
	
	
	
}
