package agile_newsagent_project;

public class RegisterExceptionHandler extends Exception{
	String message;

	public RegisterExceptionHandler(String errMessage) {
		message = errMessage;
	}

	public String getMessage() {
		return message;
	}
}
