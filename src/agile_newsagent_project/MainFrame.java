package agile_newsagent_project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class MainFrame extends JFrame implements ActionListener {
	Register user = new Register();
	private JTextField tfUserN = new JTextField();
	private JTextField tfPass = new JTextField();
	private JTextField tfConPass = new JTextField();
	private JTextField tfUType = new JTextField();
	private JTextField LoginUsername = new JTextField();
	private JTextField LoginPass = new JTextField();
	private JTextField LoginUserT = new JTextField();
	private JButton btnRegister = new JButton("Register!");
	private JButton btnLogin = new JButton("Login!");

	public MainFrame() {

		setTitle("Agile Newspaper Agency");
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(
				new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0),
						new Color(0, 0, 0)),
				"Registration", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		panel.setBounds(10, 145, 249, 238);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblUserName = new JLabel("User Name:");
		lblUserName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblUserName.setBounds(10, 26, 79, 27);
		panel.add(lblUserName);

		JLabel lblPass = new JLabel("Password:");
		lblPass.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblPass.setBounds(10, 53, 79, 27);
		panel.add(lblPass);

		JLabel lblConfirmPass = new JLabel("Confirm Password:");
		lblConfirmPass.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblConfirmPass.setBounds(10, 81, 129, 27);
		panel.add(lblConfirmPass);

		JLabel lblUserType = new JLabel("User Type:");
		lblUserType.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblUserType.setBounds(10, 110, 79, 27);
		panel.add(lblUserType);

		tfUserN.setBounds(141, 30, 86, 20);
		panel.add(tfUserN);
		tfUserN.setColumns(10);

		tfPass.setColumns(10);
		tfPass.setBounds(141, 57, 86, 20);
		panel.add(tfPass);

		tfConPass.setColumns(10);
		tfConPass.setBounds(141, 85, 86, 20);
		panel.add(tfConPass);

		tfUType.setColumns(10);
		tfUType.setBounds(141, 114, 86, 20);
		panel.add(tfUType);

		btnRegister.setBounds(81, 204, 89, 23);
		panel.add(btnRegister);
		
		JLabel lblUserType_1 = new JLabel("User Type 1 for Delivery:");
		lblUserType_1.setForeground(Color.RED);
		lblUserType_1.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		lblUserType_1.setBounds(39, 141, 162, 27);
		panel.add(lblUserType_1);
		
		JLabel lblUserType_2 = new JLabel("User Type 0 for Admin:");
		lblUserType_2.setForeground(Color.RED);
		lblUserType_2.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		lblUserType_2.setBounds(40, 166, 162, 27);
		panel.add(lblUserType_2);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(
				new TitledBorder(
						new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0),
								new Color(0, 0, 0)),
						"Login", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		panel_1.setBounds(272, 145, 249, 238);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel label = new JLabel("User Name:");
		label.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label.setBounds(10, 23, 79, 27);
		panel_1.add(label);

		JLabel label_1 = new JLabel("Password:");
		label_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_1.setBounds(10, 51, 79, 27);
		panel_1.add(label_1);

		JLabel label_2 = new JLabel("User Type:");
		label_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		label_2.setBounds(10, 79, 79, 27);
		panel_1.add(label_2);

		btnLogin.setBounds(81, 204, 89, 23);
		panel_1.add(btnLogin);

		LoginUsername = new JTextField();
		LoginUsername.setBounds(99, 27, 108, 20);
		panel_1.add(LoginUsername);
		LoginUsername.setColumns(10);

		LoginPass = new JTextField();
		LoginPass.setText("");
		LoginPass.setBounds(99, 55, 108, 20);
		panel_1.add(LoginPass);
		LoginPass.setColumns(10);

		LoginUserT = new JTextField();
		LoginUserT.setText("");
		LoginUserT.setBounds(99, 83, 108, 20);
		panel_1.add(LoginUserT);
		LoginUserT.setColumns(10);
		
		JLabel label_3 = new JLabel("User Type 1 for Delivery:");
		label_3.setForeground(Color.RED);
		label_3.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		label_3.setBounds(45, 140, 162, 27);
		panel_1.add(label_3);
		
		JLabel label_4 = new JLabel("User Type 0 for Admin:");
		label_4.setForeground(Color.RED);
		label_4.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		label_4.setBounds(55, 166, 144, 27);
		panel_1.add(label_4);

		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/news.PNG")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(197, 11, 144, 123);
		getContentPane().add(lblNewLabel);

		setSize(543, 432);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		btnRegister.addActionListener(this);
		btnLogin.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();
		if (target == btnRegister) {

			try {
				boolean r = user.registerValidation(tfUserN.getText(), tfPass.getText(), tfConPass.getText(),
						Integer.parseInt(tfUType.getText()));
				JOptionPane.showMessageDialog(null,"You have registered Succesfully");
				tfUserN.setText("");
				tfPass.setText("");
				tfConPass.setText("");
				tfUType.setText("");
			} catch (RegisterExceptionHandler e1) {
				JOptionPane.showMessageDialog(null, "Exception:" + e1.getMessage());
				e1.printStackTrace();
			}
		}

		if (target == btnLogin) {
			
			
			DetailsFrame frame = new DetailsFrame();		//opens up login page
			frame.setVisible(true);
			dispose();
		}
	}
}
