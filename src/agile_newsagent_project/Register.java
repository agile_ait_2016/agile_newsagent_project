package agile_newsagent_project;

public class Register {
	public boolean registerValidation(String name, String password, String password1, int userType)
			throws RegisterExceptionHandler {
		String pattern = "^[a-z A-Z 0-9]*$";
		boolean register = true;

		// Responsible for checking if the password contains a capital
		boolean upperCase = false;
		for (int i = 0; i < password.length(); i++) {
			if (Character.isUpperCase(password.charAt(i))) {
				upperCase = true;
			}
		}
		if ((name.length() == 0) || (password.length() == 0)) {
			register = false;
			throw new RegisterExceptionHandler("Text field is empty must have proper criteria entered");
		}
		// Checks the password is between 6 and twelve characters exclusive
		else if ((password.length() < 6) || (password.length() >= 12)) {
			register = false;
			throw new RegisterExceptionHandler("Invalid Password, must have 6-12 characters");
		}
		// Checks the password contains an uppercase letter
		else if (!upperCase) {
			register = false;
			throw new RegisterExceptionHandler("Invalid Password, must contain capital");
			// Checks if the passwords match
		} else if (!password.equals(password1)) {
			register = false;
			throw new RegisterExceptionHandler("Password mis match");
		}
		// Checks the Name is between 6 and twelve letters exclusive
		else if ((name.length() < 6) || (name.length() >= 12)) {
			register = false;
			throw new RegisterExceptionHandler("Invalid Name, must have 6-12 characters");
		}
		// check to make sure name is alphanumeric (only contains letters or
		// numbers
		else if (!name.matches(pattern)) {
			register = false;
			throw new RegisterExceptionHandler("Invalid Name, must only contain AlphaNumeric characters");
		}
		// check the user type for admin and delivery boy admin is set to one
		// and delivery boy is set to zero
		else if (userType != 1 && userType != 0) {
			register = false;
			throw new RegisterExceptionHandler("Invalid user type must be a 1 for Admin,and 0 for delivery boy");
		} else if (register) {
			DB DB = new DB();
			DB.registerValidation(name, password, password1, userType);
		}

		return register;

	}
}
