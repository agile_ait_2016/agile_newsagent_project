package agile_newsagent_project;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Bill {
	private int id;
	private Date date;
	private int amount;
	private String[] options = {"Cash","Card", "PayCheck"};
	private String selectedOption;
	private String description;
	public Bill(){}
	public Bill(int id, Date date, int amount, String description) {
		super();
		this.id = id;
		this.date = date;
		this.amount = amount;
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		if(id<0)
			this.id=0;
		else
			this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(String dates) throws DateExceptionHandler {
		String[] array = dates.split("/");
		if(Integer.parseInt(array[0])<=0 || Integer.parseInt(array[0])>31 || Integer.parseInt(array[1])<=0 || 
				Integer.parseInt(array[1])>12 || Integer.parseInt(array[2])<1970)
			throw new DateExceptionHandler("Wrong message");
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse(dates);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.date = date;
	}
	public int getAmount() {
		return amount;
	}
	public void cancelBill(int BillId){
		DB connector = new DB();
        connector.cancelBill(BillId);
	}
	public void setAmount(int amount) throws AmountHandler{
		if(amount<0)
			throw new AmountHandler("Amount not valid");
		else
			this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setPaymentOption(int option){
		if(option>=options.length || option<0)
			this.selectedOption = "No method selected";
		else
			this.selectedOption = options[option];
	}
	public String getSelectedOption(){
		return selectedOption;
	}
	public String printAll() {
		if(id<0 || date == null || amount<0 || description == null || description.equals(""))
			return null;
		else
			return "Id: "+id+"\nDate: "+date.toString()+"\nAmount: "+amount+"\nDescription: "+description + "\nPayment Option: "+selectedOption;
	}
	
}
