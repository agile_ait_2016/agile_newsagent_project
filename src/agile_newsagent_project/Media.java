package agile_newsagent_project;

public class Media {
	int id;
	String contentType;
	String name;
	String language;
	
	public Media(int id, String contentType, String name, String language) {
		super();
		this.id = id;
		this.contentType = contentType;
		this.name = name;
		this.language = language;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
}
