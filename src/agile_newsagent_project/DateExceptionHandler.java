package agile_newsagent_project;

public class DateExceptionHandler extends Exception{
	public DateExceptionHandler(String msg){
		super(msg);
	}
}
