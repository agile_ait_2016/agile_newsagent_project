package agile_newsagent_project;

import java.util.Calendar;

public class Delivery {
	
	String address = null;
	double price=0.50;
	int items;
	boolean availibleBill;
	int customerId;
	int deliveryDate;
	
	

	public String print(int id) {
		DB connector = new DB();
		String address = connector.getAddress(id);
		return address;
		
	}
	public void setDate(int date){
		deliveryDate = date;
		
	}
	
	public void setItem(int newItems){
		items = newItems;
	}
	public double getTotalPrice(){
		if(items<=3){
			return price*items;
		}
		else{
			return price*3;
		}
	}
	
	public double getPrice(){
		return price;
	}
	public void setPrice(double newPrice){
		
		price = newPrice;
	}
	
	public boolean checkAvailibleBill(){
		Calendar c = Calendar.getInstance();
		int currentDate = c.get(Calendar.DAY_OF_WEEK);
		User user = new User();
		DB connector = new DB();
		 //if(connector.getItem(user.getId()) == 0){ return false;}
		//waiting for media to become availible
		 if(currentDate==deliveryDate){
		return availibleBill;
		}
		else {return false;}
		
	}
	public void setAvailibleBill(boolean bill){
		availibleBill = bill;
	}

}
