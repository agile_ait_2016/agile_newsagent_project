package tests;

import agile_newsagent_project.Subscription;
import junit.framework.TestCase;

/**
 * @author nico
 */
public class Sub5Tests extends TestCase{

	/**
	 * test no 			= 1
	 * objective 		= test that is able to set what day of the month to receive the newspaper
	 * inputs			= subscription monthly, day 5 (the user wants to receive the 5th of each month
	 * expected output	= 5
	 */
	public void test1(){
		
		int input = 5;
		int expectedOutput = 5;
		
		Subscription subscription = new Subscription("Monthly");
		
		subscription.setPreferedDay(input);
		
		int output = subscription.getPreferedDay();
		
		assertEquals(expectedOutput, output);
		
	}
	
	/**
	 * test no 			= 2
	 * objective 		= test that is able to set what day of the month to receive the newspaper
	 * inputs			= subscription monthly, day 40
	 * expected output	= 1 (defaut value)
	 */
	public void test2(){
		
		int input = 40;
		int expectedOutput = 1;
		
		Subscription subscription = new Subscription("Monthly");
		
		subscription.setPreferedDay(input);
		
		int output = subscription.getPreferedDay();
		
		assertEquals(expectedOutput, output);
		
	}
	
	/**
	 * test no 			= 3
	 * objective 		= test that is able to set what day of the week to receive the newspaper
	 * inputs			= subscription weekly, day 1 (the user wants to receive every Monday)
	 * expected output	= 5
	 */
	public void test3(){
		
		int input = 1;
		int expectedOutput = 1;
		
		Subscription subscription = new Subscription("Daily");
		
		subscription.setPreferedDay(input);
		
		int output = subscription.getPreferedDay();
		
		assertEquals(expectedOutput, output);
		
	}
	
	/**
	 * test no 			= 4
	 * objective 		= test that is able to set what day of the week to receive the newspaper
	 * inputs			= subscription weekly, day 10 
	 * expected output	= 1 (default value)
	 */
	public void test4(){
		
		int input = 10;
		int expectedOutput = 1;
		
		Subscription subscription = new Subscription("Daily");
		
		subscription.setPreferedDay(input);
		
		int output = subscription.getPreferedDay();
		
		assertEquals(expectedOutput, output);
		
	}
}
