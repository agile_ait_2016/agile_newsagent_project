package tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import agile_newsagent_project.Bill;
import junit.framework.TestCase;

/**
 * @author jorgemanzanares
 *
 */
public class Bill2Test extends TestCase {

	/* Test Number		=	001
	 * Objective		=	test that the the details has correctly output
	 * Input(s)			=	1, 06/03/2016, 200, trying the description;
	 * Expected output	=	"Id: 1\nDate 06/03/2016\nAmount 200\nDescription: Trying the description"
	 * */
	public void test1(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, "Trying the description");

		String output = bill.printAll();

		assertEquals("Id: 1\nDate: "+date.toString()+"\nAmount: 200\nDescription: Trying the description", output);
	}

	/* Test Number		=	002
	 * Objective		=	test that the date is not null for the output bill
	 * Input(s)			=	1, null, 200, trying the description
	 * Expected output	=	null
	 * */
	public void test2(){
		Bill bill = new Bill(1, null, 200, "Trying the description");

		String output = bill.printAll();

		assertEquals(null, output);
	}
	/* Test Number		=	004
	 * Objective		=	test that the description is not null for the output bill
	 * Input(s)			=	1, new Date("06/03/2016"), 200, null
	 * Expected output	=	null
	 * */
	public void test4(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, null);

		String output = bill.printAll();

		assertEquals(null, output);
	}
	/* Test Number		=	005
	 * Objective		=	test that the description is not empty for the output bill
	 * Input(s)			=	1, new Date("06/03/2016"), 200, ""
	 * Expected output	=	null
	 * */
	public void test5(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, "");

		String output = bill.printAll();

		assertEquals(null, output);
	}
	/* Test Number		=	006
	 * Objective		=	test that the id field is not negative
	 * Input(s)			=	-5, new Date("06/03/2016"), 200, trying the description
	 * Expected output	=	null
	 * */
	public void test6(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(-5, date, 200, "");

		String output = bill.printAll();

		assertEquals(null, output);
	}
	/* Test Number		=	007
	 * Objective		=	test that the amount field is not negative
	 * Input(s)			=	5, new Date("06/03/2016"), -200, trying the description
	 * Expected output	=	null
	 * */
	public void test7(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(5, date, -200, "");

		String output = bill.printAll();

		assertEquals(null, output);
	}
}
