package tests;

import static org.easymock.EasyMock.*;

import java.util.Arrays;

import junit.framework.TestCase;

import org.easymock.*;

import agile_newsagent_project.User;
import agile_newsagent_project.UserDAO;

public class Del1Tests extends TestCase{

	UserDAO mockDao;
	User user;
	
	@Override
	public void setUp(){
		mockDao = createStrictMock(UserDAO.class);
		User.setUserDAO(mockDao);
		user = new User();
	}
	
	/**
	 * test no 			= 1
	 * objective 		= verify that the delivery details are sorted for the deliveryBoy1
	 * inputs			= User: deliveryBoy1 && deliveries: address3, address2, address1
	 * expected output	= Deliveries: address1, address2, address3
	 */
	public void test1(){
		String userName = "deliveryBoy1";
		String[] expected = new String[]{"address1","address2","address3"};
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {"address3","address2","address1"});
		replay(mockDao);
		try{
			assertEquals(Arrays.toString(expected),Arrays.toString(user.getSortedDeliveries()));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
		verify(mockDao);
	}
	
	/**
	 * test no 			= 2
	 * objective 		= verify that there is no error when the user does not have any deliveries
	 * inputs			= User: deliveryBoy1
	 * expected output	= Deliveries: {}
	 */
	public void test2(){
		String userName = "admin";
		String[] expected = new String[]{};
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {});
		replay(mockDao);
		try{
			assertEquals(Arrays.toString(expected),Arrays.toString(user.getSortedDeliveries()));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
		verify(mockDao);
	}
	
	/**
	 * test no 			= 3
	 * objective 		= verify that a sorted list is return sorted
	 * inputs			= User: deliveryBoy1 && deliveries: address1, address2, address3
	 * expected output	= Deliveries: {address1, address2, address3}
	 */
	public void test3(){
		String userName = "admin";
		String[] expected = new String[]{"address1","address2","address3"};
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {"address1","address2","address3"});
		replay(mockDao);
		try{
			assertEquals(Arrays.toString(expected),Arrays.toString(user.getSortedDeliveries()));
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
		verify(mockDao);
	}
}