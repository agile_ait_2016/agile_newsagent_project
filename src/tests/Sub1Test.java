package tests;



import agile_newsagent_project.Subscription;
import junit.framework.TestCase;

public class Sub1Test extends TestCase{

	/**
	 * test no 			= 1
	 * objective 		= be able to set what kind of subscription this person is
	 * inputs			= 1 (1 = daily, 2 = weekly, 3 = monthly)
	 * expected output	= true
	 */
	public void test1() {
		int input = 1;
		String expectedOutput = "Daily";
		
		Subscription subscription = new Subscription("Type");
		subscription.setCustomerID(1);//need to set id due to bypassed log in
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 2
	 * objective 		= be able to set what kind of subscription this person is
	 * inputs			= 2 (1 = daily, 2 = weekly, 3 = monthly)
	 * expected output	= true
	 */
	public void test2() {
		int input = 2;
		String expectedOutput = "Weekly";
		
		Subscription subscription = new Subscription("Type");
		subscription.setCustomerID(1);
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 3
	 * objective 		= be able to set what kind of subscription this person is
	 * inputs			= 3 (1 = daily, 2 = weekly, 3 = monthly)
	 * expected output	= true
	 */
	public void test3() {
		int input = 3;
		String expectedOutput = "Monthly";
		
		Subscription subscription = new Subscription("Type");
		subscription.setCustomerID(1);
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	
	/**
	 * test no 			= 4
	 * objective 		= not be able to set other(out of range) subscription
	 * inputs			= 4 (1 = daily, 2 = weekly, 3 = monthly)
	 * expected output	= true
	 */
	public void test4() {
		int input = 4;
		String expectedOutput = "Error";
		
		Subscription subscription = new Subscription("Type");
		subscription.setCustomerID(1);
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	
	/**
	 * test no 			= 5
	 * objective 		= Customer that cancel is no longer a subscriber
	 * inputs			= 0 (0 = not subscribed)
	 * expected output	= true
	 */
	public void test5() {
		int input = 0;
		String expectedOutput = "Non-Subscriber";
		
		Subscription subscription = new Subscription("Type");
		subscription.setCustomerID(1);
		subscription.setType(input);
		
		String output = subscription.getType();
		System.out.println(output);
		
		assertEquals(expectedOutput, output);
	}
}
