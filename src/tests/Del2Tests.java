package tests;

import static org.easymock.EasyMock.*;
import junit.framework.TestCase;

import org.easymock.*;

import agile_newsagent_project.User;
import agile_newsagent_project.UserDAO;

public class Del2Tests extends TestCase{

	UserDAO mockDao;
	User user;
	
	@Override
	public void setUp(){
		mockDao = createStrictMock(UserDAO.class);
		User.setUserDAO(mockDao);
		user = new User();
	}
	
	/**
	 * test no 			= 1
	 * objective 		= verify that the delivery details are printed for the deliveryBoy1
	 * inputs			= User: deliveryBoy1
	 * expected output	= User: deliveryBoy1\nDeliveries:\npackage1 to customer1(address1)\npackage2 to customer2(address2)\n
	 */
	public void test1(){
		String userName = "deliveryBoy1";
		String expected = "User: deliveryBoy1\nDeliveries:\npackage1 to customer1(address1)\npackage2 to customer2(address2)\n";
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {"package1/customer1/address1","package2/customer2/address2"});
		replay(mockDao);
		try{
			assertEquals(expected,user.printDeliveries());
		}catch(Exception e){
			fail();
		}
		verify(mockDao);
	}
	
	/**
	 * test no 			= 2
	 * objective 		= verify that the delivery details are empty for the admin user
	 * inputs			= User: admin
	 * expected output	= User: admin\nDeliveries:\nNo deliveries
	 */
	public void test2(){
		String userName = "admin";
		String expected = "User: admin\nDeliveries:\nNo deliveries";
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {});
		replay(mockDao);
		try{
			assertEquals(expected,user.printDeliveries());
		}catch(Exception e){
			fail();
		}
		verify(mockDao);
	}
	
	/**
	 * test no 			= 3
	 * objective 		= verify that an exception is thrown if the user is null
	 * inputs			= User: null
	 * expected output	= Exception with message "The user name cannot be blank or null"
	 */
	public void test3(){
		String userName = null;
		String expected = "The user name cannot be blank or null";
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {});
		replay(mockDao);
		try{
			assertEquals(expected,user.printDeliveries());
			fail();
		}catch(Exception e){
			assertEquals(expected, e.getMessage());
		}
	}
	/**
	 * test no 			= 4
	 * objective 		= verify that an exception is thrown if the user is blank
	 * inputs			= User: ""
	 * expected output	= Exception with message "The user name cannot be blank or null"
	 */
	public void test4(){
		String userName = "";
		String expected = "The user name cannot be blank or null";
		user.setName(userName);
		expect(mockDao.getDeliveries(userName)).andReturn(new String[] {});
		replay(mockDao);
		try{
			assertEquals(expected,user.printDeliveries());
			fail();
		}catch(Exception e){
			assertEquals(expected, e.getMessage());
		}
	}
}
