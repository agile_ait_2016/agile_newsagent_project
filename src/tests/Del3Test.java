package tests;

import java.util.Calendar;

import agile_newsagent_project.Delivery;
import junit.framework.TestCase;
import sun.util.calendar.BaseCalendar.Date;

public class Del3Test extends TestCase {
	/**
	 * test no 			= 1
	 * objective 		= no alert if there is no bill.
	 * inputs			= false
	 * expected output	= false
	 */
	public void test1() {
		boolean input = false;
		boolean expectedOutput = false;
		
		Delivery delivery = new Delivery();
		delivery.setAvailibleBill(input);
		
		boolean output = delivery.checkAvailibleBill();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 2
	 * objective 		= alert if there is a bill.
	 * inputs			= true
	 * expected output	= true
	 */
	public void test2() {
		boolean input = true;
		boolean expectedOutput = true;
		
		Calendar c = Calendar.getInstance();
		int currentDate = c.get(Calendar.DAY_OF_WEEK);//set delivery date to current date
		Delivery delivery = new Delivery();
		delivery.setAvailibleBill(input);
		delivery.setDate(currentDate);
		
		boolean output = delivery.checkAvailibleBill();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 3
	 * objective 		= no delivery if not on the date.
	 * inputs			= true
	 * expected output	= true
	 */
	public void test3() {
		boolean input = true;
		boolean expectedOutput = false;
		
		Calendar c = Calendar.getInstance();
		int currentDate = c.get(Calendar.DAY_OF_WEEK+1);//setting value to current date+1
		Delivery delivery = new Delivery();
		delivery.setAvailibleBill(input);
		delivery.setDate(currentDate);
		
		boolean output = delivery.checkAvailibleBill();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 4
	 * objective 		= even on date, if no bills, no delivery.
	 * inputs			= true
	 * expected output	= true
	 */
	public void test4() {
		boolean input = false;
		boolean expectedOutput = false;
		
		Calendar c = Calendar.getInstance();
		int currentDate = c.get(Calendar.DAY_OF_WEEK);//setting value to current date
		Delivery delivery = new Delivery();
		delivery.setAvailibleBill(input);
		delivery.setDate(currentDate);
		
		boolean output = delivery.checkAvailibleBill();
		
		assertEquals(expectedOutput, output);
	}

}
