package tests;

import agile_newsagent_project.Delivery;
import junit.framework.TestCase;

public class Del4Test extends TestCase {
	
	/**
	 * test no 			= 1
	 * objective 		= be able print details
	 * inputs			= 1 // This is the user ID
	 * expected output	= true
	 */
	public void test1() {
		int input = 1;
		String expectedOutput = null;
		
		Delivery delivery = new Delivery();
		
		String output = delivery.print(input);
		
		assertNotSame(expectedOutput, output);
		System.out.println(output);
	}

	/**
	 * test no 			= 2
	 * objective 		= not be able print details if user not exist
	 * inputs			= 500 // This is the user ID
	 * expected output	= same
	 */
	public void test2() {
		int input = 500;
		String expectedOutput = null;
		
		Delivery delivery = new Delivery();
		
		String output = delivery.print(input);
		
		assertNull(expectedOutput, output);
	}
	

}
