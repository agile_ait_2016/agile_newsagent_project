package tests;

import static org.junit.Assert.*;

import agile_newsagent_project.Subscription;
import junit.framework.TestCase;

public class SubTest4  extends TestCase{

	/**
	 * test no          = 001 			
	 * objective 		= be able to modify the type of subscription
	 * inputs			= 0 (0 = daily, 1 = weekly, 2 = monthly)
	 * expected output	= true
	 */
	public void test1() {
		int input = 0;
		
		String expectedOutput = "Daily";
		
		Subscription subscription = new Subscription("Type");
		
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 002
	 * objective 		= be able to modify the type of subscription
	 * inputs			= 1 (0 = daily, 1 = weekly, 2 = monthly)
	 * expected output	= true
	 */
	public void test2() {
		int input = 1;
		String expectedOutput = "Weekly";
		
		Subscription subscription = new Subscription("Type");
		
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 003
	 * objective 		= be able to modify the type of subscription
	 * inputs			= 2 (0 = daily, 1 = weekly, 2 = monthly)
	 * expected output	= true
	 */
	public void test3() {
		int input = 2;
		String expectedOutput = "Monthly";
		
		Subscription subscription = new Subscription("Type");
		
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	
	/**
	 * test no 			= 004
	 * objective 		= not be able to modify the type of subscription
	 * inputs			= 4 (0 = daily, 1 = weekly, 2 = monthly)
	 * expected output	= false
	 */
	public void test4() {
		int input = 4;
		String expectedOutput = "Non-Subscriber";
		
		Subscription subscription = new Subscription("Type");
		
		subscription.setType(input);
		
		String output = subscription.getType();
		
		assertEquals(expectedOutput, output);
	}
	

}
