package tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import agile_newsagent_project.Bill;
import agile_newsagent_project.DateExceptionHandler;
import junit.framework.TestCase;

/**
 * @author Jorge Manzanares
 *
 */
public class Bill1Tests extends TestCase {

	/* Test Number		=	1
	 * Objective		=	Test that the id for billing has correctly changed.
	 * Input(s)			=	3
	 * Expected output	=	3
	 * */
	public void test1(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1,date,200,"Checked that I can change the ID");
		bill.setId(3);
		int output = bill.getId();
		assertEquals(3, output);
	}
	
	/* Test Number		=	2
	 * Objective		=	Test that the id for billing is more than 0.
	 * Input(s)			=	-18
	 * Expected output	=	0
	 * */
	public void test2(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(-18,date,200,"Checked that I can change the ID");
		bill.setId(-18);
		int output = bill.getId();
		assertEquals(0, output);
	}
	
	/* Test Number		=	3
	 * Objective		=	Test that the day of the date has not correctly introduced.
	 * Input(s)			=	47/03/2001
	 * Expected output	=	ParseException
	 * */
	public void test3(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(8,date,200,"Checked that I can change the ID");
		try {
			bill.setDate("47/03/2001");
			fail();
		} catch (DateExceptionHandler e) {
			assertEquals("Wrong message", e.getMessage());
		}
		
	}
	
	/* Test Number		=	4
	 * Objective		=	Test that the day of the date has not correctly introduced.
	 * Input(s)			=	47/03/2001
	 * Expected output	=	ParseException
	 * */
	public void test4(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(8,date,200,"Checked that I can change the ID");
		try {
			bill.setDate("47/03/2001");
			fail();
		} catch (DateExceptionHandler e) {
			assertEquals("Wrong message", e.getMessage());
		}
		
	}
	
	/* Test Number		=	5
	 * Objective		=	Test that the month of the date has not correctly introduced.
	 * Input(s)			=	20/59/2001
	 * Expected output	=	ParseException
	 * */
	public void test5(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(8,date,200,"Checked that I can change the ID");
		try {
			bill.setDate("20/59/2001");
			fail();
		} catch (DateExceptionHandler e) {
			assertEquals("Wrong message", e.getMessage());
		}
	}
	//////BOUNDRY VALUE//////
	/* Test Number		=	6
	 * Objective		=	Test that the year of the date has not correctly introduced.
	 * Input(s)			=	20/59/1969
	 * Expected output	=	ParseException
	 * */
	public void test6(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(8,date,200,"Checked that I can change the ID");
		try {
			bill.setDate("20/59/1969");
			fail();
		} catch (DateExceptionHandler e) {
			assertEquals("Wrong message",e.getMessage());
		}
	}
	/* Test Number		=	7
	 * Objective		=	Test that the year of the date has correctly introduced.
	 * Input(s)			=	20/10/1970
	 * Expected output	=	ParseException
	 * */
	public void test7(){
		Date date = new Date();
		Date output = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
			output = format.parse("20/10/1970");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(8,date,200,"Checked that I can change the ID");
		try {
			bill.setDate("20/10/1970");
			assertEquals(bill.getDate(),output);
		} catch (DateExceptionHandler e) {
			fail();
		}
	}
	
	/* Test Number		=	8
	 * Objective		=	Test that the year of the date has correctly introduced.
	 * Input(s)			=	20/10/1971
	 * Expected output	=	ParseException
	 * */
	public void test8(){
		Date date = new Date();
		Date output = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
			output = format.parse("20/10/1971");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(8,date,200,"Checked that I can change the ID");
		try {
			bill.setDate("20/10/1971");
			assertEquals(bill.getDate(),output);
		} catch (DateExceptionHandler e) {
			fail();
		}
	}
	
}
