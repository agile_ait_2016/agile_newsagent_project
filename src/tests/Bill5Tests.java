package tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import agile_newsagent_project.AmountHandler;
import agile_newsagent_project.Bill;
import agile_newsagent_project.DateExceptionHandler;
import junit.framework.TestCase;

public class Bill5Tests extends TestCase {
	/**
	 * test no          = 001 			
	 * objective 		= be able to change ID of customer's bill
	 * inputs			= 4
	 * expected output	= 4
	 */

	public void test1(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1,date,200,"Checked that I can change the ID");
		bill.setId(4);
		int output = bill.getId();
		assertEquals(4, output);
	}
	/**
	 * test no          = 002			
	 * objective 		= be able to change amount of customer's bill
	 * inputs			= 300
	 * expected output	= 300
	 * @throws AmountHandler 
	 */
	public void test2() throws AmountHandler{
	Date date = new Date();
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	try {
		date = format.parse("06/03/2016");
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Bill bill = new Bill(1,date,200,"Checked that I can change the ID");
	bill.setAmount(300);
	int output = bill.getAmount();
	assertEquals(300,output);
}
	/**
	 * test no          = 003			
	 * objective 		= be able to change data of customer's bill
	 * inputs			= 06/04/2016
	 * expected output	= 06/04/2016
	 * @throws DateExceptionHandler 
	 */
	public void test3() throws DateExceptionHandler{
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1,date,200,"Checked that I can change the ID");
		bill.setDate("06/04/2016");
		Date output=bill.getDate();
		assertEquals("06/04/2016",output);
	}
}
