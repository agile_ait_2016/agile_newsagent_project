package tests;

import agile_newsagent_project.Delivery;
import junit.framework.TestCase;

public class Del5Test extends TestCase {
	
	/**
	 * test no 			= 1
	 * objective 		= be able get price per book
	 * expected output	= 50 cent
	 */
	public void test1() {
		double expectedOutput = 0.50;
		
		Delivery delivery = new Delivery();
		
		double output = delivery.getPrice();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 2
	 * objective 		= max price is 1.50 for more than 3 items
	 * expected output	= 1.50 cent
	 */
	public void test2() {
		double expectedOutput = 1.50;
		
		Delivery delivery = new Delivery();
		delivery.setItem(6);
		Double output = delivery.getTotalPrice();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 3
	 * objective 		= Should have no price if no item were bought
	 * expected output	= 0 cent
	 */
	public void test3() {
		double expectedOutput = 0;
		
		Delivery delivery = new Delivery();
		delivery.setItem(0);
		Double output = delivery.getTotalPrice();
		
		assertEquals(expectedOutput, output);
	}

}
