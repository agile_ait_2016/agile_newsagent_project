package tests;

import static org.junit.Assert.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import agile_newsagent_project.Bill;
import agile_newsagent_project.Subscription;
import junit.framework.TestCase;

public class Sub3Test extends TestCase{


	/**
	 * test no          = 001 			
	 * objective 		= send the request of customerID
	 * inputs			= integer number
	 * expected output	= true
	 */
	public void test1() {
		int input = 1234;
		
		int expectedOutput = 1234;
		
		Subscription subscription = new Subscription("customer");
		
		subscription.setCustomerID(input);
		
		int output = subscription.getCustomerID();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no          = 002 			
	 * objective 		= send the request of mediaID
	 * inputs			= integer number
	 * expected output	= true
	 */
	public void test2() {
		int input = 1234;
		int expectedOutput = 1234;
		
		Subscription subscription = new Subscription("media");
		
		subscription.setmediaID(input);
		
		int output = subscription.getmediaID();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no          = 003			
	 * objective 		= get the data of order
	 * inputs			= data
	 * expected output	= true
	 */
	public void test3() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, "Trying the description");
		
		String output = bill.printAll();
		
		assertEquals("Id: 1\nDate: "+date.toString()+"\nAmount: 200\nDescription: Trying the description", output);
	}
	/**
	 * test no          = 004			
	 * objective 		= get the data of order
	 * inputs			= data
	 * expected output	= true
	 */
	public void test4(){
		int input = 1234;
		
		int expectedOutput = 1234;
		
		Subscription subscription = new Subscription("subscription");
		
		subscription.setsubscriptionID(input);
		
		int output = subscription.getsubscriptionID();
		
		assertEquals(expectedOutput, output);
	}
}
