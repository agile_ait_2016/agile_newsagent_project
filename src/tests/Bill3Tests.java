package tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import agile_newsagent_project.AmountHandler;
import agile_newsagent_project.Bill;
import agile_newsagent_project.Customer;
import junit.framework.TestCase;

public class Bill3Tests extends TestCase {

	/* Test Number		=	001
	 * Objective		=	test that the the bill includes a delivery name.
	 * Input(s)			=	Bill(1, 06/03/2016, 200, trying the description), Customer(1, "Jorge", "Manzanares", "croi oige", "AIT", 1, "+34666908644", "ait@student.com")
	 * Expected output	=	"Jorge Manzanares"
	 * */
	public void test1(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Customer c = new Customer(1, "Jorge", "Manzanares", "croi oige", "AIT", 1, "+34666908644", "ait@student.com");
		Bill bill = new Bill(1, date, 200, "Trying the description");
		c.addBill(bill);
		String output = c.getName()+" "+c.getSurname();
		
		assertEquals("Jorge Manzanares", output);
	}
	
	/* Test Number		=	002
	 * Objective		=	test that the delivery name is not null for the customers bill.
	 * Input(s)			=	Bill(1, 06/03/2016, 200, trying the description), Customer(1, null, null, "croi oige", "AIT", 1, "+34666908644", "ait@student.com")
	 * Expected output	=	No name entered
	 * */
	public void test2(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Customer c = new Customer(1, null, null, "croi oige", "AIT", 1, "+34666908644", "ait@student.com");
		Bill bill = new Bill(1, date, 200, "Trying the description");
		c.addBill(bill);
		String output = c.getName();
		
		assertEquals("No name entered", output);
	}
	/* Test Number		=	003
	 * Objective		=	Test the select payment option.
	 * Input(s)			=	Bill(1, 06/03/2016, 200, trying the description), selectedOption=2
	 * Expected output	=	Card
	 * */
	public void test3(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, "Trying the description");
		bill.setPaymentOption(2);
		String output = bill.getSelectedOption();
		
		assertEquals("PayCheck", output);
	}
	
	/* Test Number		=	004
	 * Objective		=	Test the select payment option.
	 * Input(s)			=	Bill(1, 06/03/2016, 200, trying the description), selectedOption=6
	 * Expected output	=	No method selected
	 * */
	public void test4(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, "Trying the description");
		bill.setPaymentOption(6);
		String output = bill.getSelectedOption();
		assertEquals("No method selected", output);
	}
	/* Test Number		=	005
	 * Objective		=	Test the amount.
	 * Input(s)			=	Bill(1, 06/03/2016, -2, trying the description)
	 * Expected output	=	The amount cannot be negative
	 * */
	public void test5(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1, date, 200, "Trying the description");
		try {
			bill.setAmount(-20);
			fail();
		} catch (AmountHandler e) {
			System.out.println("asdf");
			assertEquals("Amount not valid", e.getMessage());
			e.printStackTrace();
		}
	}
	/* Test Number		=	006
	 * Objective		=	test that the amount
	 * Input(s)			=	300
	 * Expected output	=	300
	 * */
	public void test6() throws AmountHandler{
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(5, date, 200, "");
		bill.setAmount(300);
		int output = bill.getAmount();
		
		assertEquals(300, output);
	}
}
