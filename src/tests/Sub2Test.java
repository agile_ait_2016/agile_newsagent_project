package tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import agile_newsagent_project.Holiday;
import junit.framework.TestCase;

public class Sub2Test extends TestCase{

	/**
	 * test no 			= 1
	 * objective 		= be able to set a user on holidays
	 * inputs			= true
	 * expected output	= true
	 * @throws ParseException 
	 */
	public void test1() throws ParseException {
		boolean input = true;
		boolean expectedOutput = true;
		Date start = new SimpleDateFormat("yyyy-MM-dd").parse("2011-01-18");
		Date stop = new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-3");		
		Holiday holiday = new Holiday(start, stop);
		holiday.setid(1); // setting customer ID
		holiday.setHoliday(input);
		boolean output = holiday.getHoliday();
		
		assertEquals(expectedOutput, output);
	}

	/**
	 * test no 			= 2
	 * objective 		= be able to set a user off holidays
	 * inputs			= true
	 * expected output	= true
	 * @throws ParseException 
	 */
	public void test2() throws ParseException {
		boolean input = false;
		boolean expectedOutput = false;		
		Holiday holiday = new Holiday();
		holiday.setid(1); // setting customer ID
		holiday.setHoliday(input);
		boolean output = holiday.getHoliday();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 3
	 * objective 		= not be able to receive newspapers on holiday
	 * inputs			= true
	 * expected output	= false
	 * @throws ParseException 
	 */
	public void test3() throws ParseException {
		boolean input = true;
		boolean expectedOutput = false;
		Date start = new SimpleDateFormat("yyyy-MM-dd").parse("2011-01-18");
		Date stop = new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-3");		
		Holiday holiday = new Holiday();
		holiday.setDate(start, stop, input);
		holiday.setid(1); // setting customer ID
		
		boolean output = holiday.getNewspaper();
		
		assertEquals(expectedOutput, output);
	}
	/**
	 * test no 			= 4
	 * objective 		= be able to receive newspapers when not on holidays
	 * inputs			= true
	 * expected output	= false
	 * @throws ParseException 
	 */
	public void test4() throws ParseException {
		boolean input = false;
		boolean expectedOutput = true;
		
		Holiday holiday = new Holiday();
	
		holiday.setHoliday(input);
		boolean output = holiday.getNewspaper();
		
		assertEquals(expectedOutput, output);
	}
}
