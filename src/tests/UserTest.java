package tests;

import agile_newsagent_project.Register;
import agile_newsagent_project.RegisterExceptionHandler;
import agile_newsagent_project.User;
import junit.framework.TestCase;

// Author Jason Delaney
//User Story: REG1
public class UserTest extends TestCase {
	// Register
	// Test NO:1
	// Objective: Enter a user name with 6 characters and Password with a
	// capital and and at least 6 character now more than 12 (Valid,Valid,Valid)
	// Inputs: user name = "JasonD", Password ="Password1", PasswordConfirm
	// ="Password1, userType=0.
	// Expected output: registration= true;
	public void testRegister001() {
		Register user = new Register();
		try {
			assertTrue(user.registerValidation("JasonD", "Password1", "Password1", 0));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Register
	// Test NO:2
	// Objective: Enter a user name with 5 characters and Password with
	// a capital and and at least 6 no more than 12(Invalid,Valid,Valid)
	// Inputs: user name = "Jason", Password ="Password1",PasswordConfirm
	// ="Password1, userType=0.
	// Expected output: registration= false;
	public void testRegister002() {
		Register user = new Register();
		try {
			user.registerValidation("Jason", "Password1", "Password1", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid Name, must have 6-12 characters", ExceptionHandler.getMessage());
		}
	}

	// Register
	// Test NO:3
	// Objective: Enter a user name with 13 characters and Password with
	// a capital and and at least 6 no more than 12(Invalid,Valid,Valid)
	// Inputs: user name = "Jason12345678", Password
	// ="Password1",PasswordConfirm ="Password1", userType=0.
	// Expected output: registration= false;
	public void testRegister003() {
		Register user = new Register();
		try {
			user.registerValidation("Jason12345678", "Password1", "Password1", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid Name, must have 6-12 characters", ExceptionHandler.getMessage());
		}
	}

	// Register
	// Test NO:4
	// Objective: Enter a user name with 6-12 characters and a Password with
	// a capital and and 5 characters (Valid,Invalid,Valid)
	// Inputs: user name = "JasonD", Password ="Pass1" ,PasswordConfirm ="Pass1,
	// userType=0.
	// Expected output: registration= false;
	public void testRegister004() {
		Register user = new Register();
		try {
			user.registerValidation("JasonD", "Pass1", "Pass1", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid Password, must have 6-12 characters", ExceptionHandler.getMessage());
		}

	}

	// Register
	// Test NO:5
	// Objective: Enter a user name with 6-12 characters and a Password with
	// a capital and 13 characters (Valid,Invalid,Valid)
	// Inputs: user name = "JasonD", Password ="Pass123456789", PasswordConfirm
	// ="Pass123456789,0
	// Expected output: registration= false;
	public void testRegister005() {
		Register user = new Register();
		try {
			user.registerValidation("JasonD", "Pass123456789", "Pass123456789", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid Password, must have 6-12 characters", ExceptionHandler.getMessage());
		}
	}

	// Register
	// Test NO:6
	// Objective: Enter a user name with 6-12 characters and a Password with
	// no capital and and 6-12 characters (Valid,Invalid,Valid)
	// Inputs: user name = "JasonD", Password ="Password1" ,PasswordConfirm
	// ="password1.,0
	// Expected output: registration= false;
	public void testRegister006() {
		Register user = new Register();
		try {
			user.registerValidation("JasonD", "password1", "password1", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid Password, must contain capital", ExceptionHandler.getMessage());
		}
	}

	// Test No: 7
	// Objective: Enter a user name with 6-12 characters and a Password with
	// capital and and 6-12 characters, password1 and passwordConfirm dont match
	// (Valid,InvalidValid)
	// Inputs: user name = "JasonD", Password ="Password1" ,PasswordConfirm
	// ="password2..userType=0.
	// Expected Output: register = false

	public void testRegister007() {
		Register user = new Register();
		try {
			user.registerValidation("JasonD", "Password1", "Password2", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Password mis match", ExceptionHandler.getMessage());
		}

	}
	// Test No: 8
	// Objective: Enter a user name with 6-12 characters and a Password with
	// capital and and 6-12 characters, password1 and passwordConfirm match
	// (Valid,Valid,Valid)
	// Inputs: user name = "JasonD", Password ="Password1" ,PasswordConfirm
	// ="Password1..userType=0.
	// Expected Output: register = true

	public void testRegister008() {
		Register user = new Register();
		try {
			assertTrue(user.registerValidation("JasonD", "Password1", "Password1", 0));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}

	}

	// Test No: 9
	// Objective: Check that username contains only alphanumerics
	// (Valid,Valid,Valid)
	// Inputs: user name = "JasonD" Password ="Password1" ,PasswordConfirm
	// ="Password1..userType=0.
	// Expected Output: register = true
	public void testRegister009() {
		Register user = new Register();
		try {
			assertTrue(user.registerValidation("JasonD", "Password1", "Password1", 0));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}

	}
	// Test No: 10
	// Objective: Check that username contains non-alphanumerics
	// (Invalid,Valid,Valid)
	// Inputs: user name = "JasonD&@" Password ="Password1" ,PasswordConfirm
	// ="Password1..userType=0.
	// Expected Output: register = fail

	public void testRegister010() {
		Register user = new Register();
		try {
			user.registerValidation("JasonD&@", "Password1", "Password1", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid Name, must only contain AlphaNumeric characters", ExceptionHandler.getMessage());
		}

	}

	// Test No: 11
	// Objective: Check that username field and password feild are not empty
	// (Valid,Valid,Valid)
	// Inputs: user name = "JasonD" Password ="Password1" ,PasswordConfirm
	// ="Password1"..userType=0.
	// Expected Output: register = true
	public void testRegister011() {
		Register user = new Register();
		try {
			assertTrue(user.registerValidation("JasonD", "Password1", "Password1", 0));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}

	}

	// Test No: 12
	// Objective: Check that username field and password feild are not empty
	// (inValid,inValid,Valid)
	// Inputs: user name = "" Password ="" ,PasswordConfirm =""..userType=0.
	// Expected Output: register = false
	public void testRegister012() {
		Register user = new Register();
		try {
			user.registerValidation("", "", "", 0);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Text field is empty must have proper criteria entered", ExceptionHandler.getMessage());
		}

	}

	// Test No: 13
	// Objective: check that the user type is correctly set,
	// Inputs: user name ="JasonD", Password="Password1",
	// PasswordConfirm="Password1" userType="0"
	// Expected output: register= true.
	public void testRegister013() {
		Register user = new Register();
		try {
			assertTrue(user.registerValidation("JasonD", "Password1", "Password1", 0));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}

	}

	// Test No: 14
	// Objective: check that the user type is notcorrectly set,
	// Inputs: user name ="JasonD", Password="Password1",
	// PasswordConfirm="Password1" userType="2"
	// Expected output: register= false.
	public void testRegister014() {
		Register user = new Register();
		try {
			user.registerValidation("JasonD", "Password1", "Password1", 2);
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("Invalid user type must be a 1 for Admin,and 0 for delivery boy", ExceptionHandler.getMessage());
		}
	}

}
