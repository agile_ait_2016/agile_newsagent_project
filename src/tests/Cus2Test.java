package tests;

import agile_newsagent_project.AddCustomer;
import agile_newsagent_project.Subscription;
import junit.framework.TestCase;

public class Cus2Test extends TestCase
{	
	/**
	 * test no 			= 1
	 * objective 		= Verify that at least one of the customers details fields (address) can be changed
	 * inputs			= id = "1", address = "Edendery"
	 * expected output	= true
	 */
	public void test01()
	{
		AddCustomer c = new AddCustomer();
		
		try
		{
			assertTrue(c.updateCusDetails("Des", "Norman", "Edenderry"));
		}
		catch(Exception e)
		{
			assertSame("Error updating details", e.getMessage());
		}
	}
	
	/**
	 * test no 			= 2
	 * objective 		= Verify that customers subscription can be changed.
	 * inputs			= id = "1", mediaID = 0, subscription = "Playboy"
	 * expected output	= true
	 */
	public void test02()
	{
		Subscription s = new Subscription();
		
		try
		{
			assertTrue(s.updateSubscription(0, 0, "Playboy"));
		}
		catch(Exception e)
		{
			assertSame("Error updating subscription", e.getMessage());
		}
	}
}