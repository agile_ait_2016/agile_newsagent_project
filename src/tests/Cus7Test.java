package tests;

import agile_newsagent_project.AddCustomer;
import agile_newsagent_project.RegisterExceptionHandler;
import junit.framework.TestCase;

//Author Jason Delaney,
//User Story: CUS7
public class Cus7Test extends TestCase {
	// Delete Customer
	// Test NO:1
	// Objective:verify can delete customer by id, delete customer from database
	// active table, insert into new table for past customers
	// Inputs: id="1"(valid)
	// Expected output:customer removed from DB, and forwarded to new table, true
	public void testCustomerDelete001() {
		AddCustomer customer = new AddCustomer();
		try {
			assertTrue(customer.removeCustomer(1));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}
	}
	// Delete Customer
		// Test NO:2
		// Objective:verify exception is thrown with invalid ID
		// Inputs: id="-1",  (invalid
		// Expected output:customer not removed from DB, false
		public void testCustomerDelete002() {
			AddCustomer customer = new AddCustomer();
			try {
				customer.removeCustomer(-1);
				fail("Exception expected");
			} catch (RegisterExceptionHandler ExceptionHandler) {
				assertSame("Not a valid Id", ExceptionHandler.getMessage());
			}
		}
		// Delete Customer
		// Test NO:3
		// Objective:verify can delete customer by name, delete customer from database
		// active table, insert into new table for past customers
		// Inputs:  name="jasonD" (valid)
		// Expected output:customer removed from DB, and forwarded to new table, true
		public void testCustomerDelete003() {
			AddCustomer customer = new AddCustomer();
			try {
				assertTrue(customer.removeCustomer( "JasonD", "Delaney"));
			} catch (RegisterExceptionHandler e) {
				fail("Exception not expected");
			}
		}
		// Delete Customer
			// Test NO:4
			// Objective:verify exception is thrown with invalid Name
			// Inputs: name="jasonD" (invalid)
			// Expected output:customer not removed from DB, false
			public void testCustomerDelete004() {
				AddCustomer customer = new AddCustomer();
				try {
					customer.removeCustomer( "","");
					fail("Exception expected");
				} catch (RegisterExceptionHandler ExceptionHandler) {
					assertSame("Not a valid name", ExceptionHandler.getMessage());
				}
			}
	
	
}
