package tests;

import agile_newsagent_project.Login;
import agile_newsagent_project.RegisterExceptionHandler;
import junit.framework.TestCase;


public class Log2Test extends TestCase
{
	/**
	 * test no 			= 1
	 * objective 		= Verify the user can log in with the correct username and password
	 * inputs			= username = "Desmond", password = "qwerty", userType = "1"
	 * expected output	= true
	 */
	public void test1()
	{
		Login l = new Login();
		
		try
		{
			assertTrue(l.login("Desmond", "qwerty", 1));
		}
		catch (RegisterExceptionHandler e)
		{
			fail("Exception not expected");
		}
	}
	
	/**
	 * test no 			= 2
	 * objective 		= Verify that the user cannot log in upon wrong username & password
	 * inputs			= username = "John", password = "asdfgh", userType = "1"
	 * expected output	= false
	 */
	public void test2()
	{
		Login l = new Login();
		
		try
		{
			assertTrue(l.login("Johnnyy", "asdfgh", 1));
		}
		catch(RegisterExceptionHandler registerExceptionHandler)
		{
			registerExceptionHandler.printStackTrace();
			fail("Exception expected");
			assertSame("Invalid Username and password", registerExceptionHandler.getMessage());
		}
	}
	
	/**
	 * test no 			= 3
	 * objective 		= Verify that the user cannot user the system without logging in
	 * inputs			= username = "", password = "", userType = "1"
	 * expected output	= false
	 */
	public void test3()
	{
		Login l = new Login();
		
		try
		{
			assertFalse(l.login("", "", 1));
			
		}
		catch(RegisterExceptionHandler registerExceptionHandler)
		{
			assertSame("You must log in to use the system", registerExceptionHandler.getMessage());
			fail("Exception expected");
		}
	}
	
	/**	
	 * test no 			= 4
	 * objective 		= Verify length of the username and password is between 6-12 (inclusive) characters long
	 * inputs			= username = "Desmond", password = "qwerty", userType = "1"
	 * expected output	= true
	 */
	public void test4()
	{
		Login l = new Login();
		
		try
		{
			assertTrue(l.login("Abcdefg", "lkjhgfd", 1));
		}
		catch (RegisterExceptionHandler e)
		{
			assertSame("Invalid Password, must have 6-12 characters", e.getMessage());
			fail("Exception expected");
		}
	}
	
	/**	
	 * test no 			= 5
	 * objective 		= Verify username and password uses only alphanumeric characters
	 * inputs			= username = "Desmond123", password = "qwerty123", userType = "1"
	 * expected output	= true
	 */
	public void test5()
	{
		Login l = new Login();
		
		try
		{
			assertTrue(l.login("Desmond123", "qwerty123", 1));
		}
		catch (RegisterExceptionHandler e)
		{
			assertSame("Invalid, username & password must be only numbers and letters", e.getMessage());
			fail("Exception expected");
		}
	}
	
	/**	
	 * test no 			= 6
	 * objective 		= Verify password must contain a capital letter
	 * inputs			= username = "Desmond123", password = "Qwerty", userType = "1"
	 * expected output	= true
	 */
	public void test6()
	{
		Login l = new Login();
		
		try
		{ 
			l.login("Desmond123", "Qwerty", 1);
		}
		catch (RegisterExceptionHandler e)
		{
			assertSame("Invalid, password must contain a capitol character", e.getMessage());
			fail("Exception expected");
		}
	}
}