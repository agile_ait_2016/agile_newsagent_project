package tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import agile_newsagent_project.Bill;
import agile_newsagent_project.DateExceptionHandler;
import junit.framework.TestCase;

public class Bill4Tests extends TestCase {
	/**
	 * test no          = 001 			
	 * objective 		= be able to get BillId by Customer name
	 * inputs			= 1
	 * expected output	= 4
	 */

	public void test1(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1,date,200,"Checked that I can change the ID");
		bill.setId(4);
		int output = bill.getId();
		assertEquals(4, output);
	}
	/**
	 * test no          = 002		
	 * objective 		= be able to cancel Bill by BillId
	 * inputs			= 1
	 * expected output	= null
	 */

	public void test2(){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = format.parse("06/03/2016");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bill bill = new Bill(1,date,200,"Checked that I can change the ID");
		bill.cancelBill(1);
		int output = bill.getId();
		assertEquals(null, output);
	}
}
