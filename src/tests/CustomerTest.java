package tests;

import agile_newsagent_project.AddCustomer;
import agile_newsagent_project.RegisterExceptionHandler;
import junit.framework.TestCase;

//Author Jason Delaney, Desmond Norman
//User Story: CUS8
public class CustomerTest extends TestCase {
	// Add Customer
	// Test NO:1
	// Objective: Verify that first name has to be inserted, verify that surname
	// has to be inserted(Valid,Valid,Valid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user true;
	public void testCustomerDetails001() {
		AddCustomer customer = new AddCustomer();
		try {
			assertTrue(customer.addDetails("Jason", "Delaney", "Apt5", "Main St", 1, "0879897898", "j@live.ie"));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Add Customer
	// Test NO:2
	// Objective: Verify that first name has to be inserted, throw exception
	// as no username entered(Invalid,Valid,Valid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "", Surname: "Delaney", "Apt5", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails002() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("", "Delaney", "Apt5", "Main St", 1, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter name",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:3
	// Objective: Verify that surename has to be inserted, throw exception
	// as no name entered(Valid,inValid,Valid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "", "Apt5", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails003() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "", "Apt5", "Main St", 1, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter name",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:4
	// Objective: Verify that surename has to be inserted, throw exception
	// as no name entered, verify surname has to be inserted throw
	// exception(inValid,inValid,Valid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "", "Apt5", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails004() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("", "", "Apt5", "Main St", 1, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter name",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:5
	// Objective: Verify that address1 and address2 are inserted throw exception
	// if no field entered
	// has to be inserted(Valid,Valid,Valid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user true;
	public void testCustomerDetails005() {
		AddCustomer customer = new AddCustomer();
		try {
			assertTrue(customer.addDetails("Jason", "Delaney", "Apt5", "Main St", 1, "0879897898", "j@live.ie"));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Add Customer
	// Test NO:6
	// Objective: Verify that address has to be inserted, throw exception
	// as no name entered, verify surname has to be inserted throw
	// exception(Valid,Valid,inValid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails006() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "Delaney", "", "Main St", 1, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter address",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:7
	// Objective: Verify that address has to be inserted, throw exception
	// as no name entered, verify surname has to be inserted throw
	// exception(Valid,Valid,Valid,inValid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "",
	// "1","0879897898",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails007() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "Delaney", "Apt5", "", 1, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter address",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:8
	// Objective: Verify that address has to be inserted, throw exception
	// as noting entered, verify surname has to be inserted throw
	// exception(Valid,Valid,inValid,inValid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "", "",
	// "1","0879897898",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails008() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "Delaney", "", "", 1, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter address",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:9
	// Objective: Verify that telephone has to be inserted, throw exception
	// as noting entered, verify surname has to be inserted throw
	// exception(Valid,Valid,Valid,Valid,Valid,inValid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "Main St",
	// "1","",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails009() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "Delaney", "Apt5", "Main St", 1, "", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("text field is empty must be have proper criteria entered, enter phone no",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:10
	// Objective: Verify that zone id is number
	// 1,2,3,4(Valid,Valid,Valid,Valid,Valid,Valid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "Main St",
	// "1","0879897898",j@live.ie
	// Expected output: add user true;
	public void testCustomerDetails010() {
		AddCustomer customer = new AddCustomer();
		try {
			assertTrue(customer.addDetails("Jason", "Delaney", "Apt5", "Main St", 1, "0879897898", "j@live.ie"));
		} catch (RegisterExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Add Customer
	// Test NO:11
	// Objective: Verify that zone ID is valid must be
	// 1,2,3,4(Valid,Valid,Valid,Valid,inValid,inValid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "Main St",
	// "0","",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails011() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "Delaney", "Apt5", "Main St", 0, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("ZoneID must be a number from 1-4, enter number 1,2,3,4 for zoneID",
					ExceptionHandler.getMessage());
		}
	}

	// Add Customer
	// Test NO:12
	// Objective: Verify that zone ID is valid must be
	// 1,2,3,4(Valid,Valid,Valid,Valid,inValid,inValid,Valid)
	// Inputs: FirstName: "Jason", Surname: "Delaney", "Apt5", "Main St",
	// "0","",j@live.ie
	// Expected output: add user false;
	public void testCustomerDetails012() {
		AddCustomer customer = new AddCustomer();
		try {
			customer.addDetails("Jason", "Delaney", "Apt5", "Main St", 5, "0879897898", "j@live.ie");
			fail("Exception expected");
		} catch (RegisterExceptionHandler ExceptionHandler) {
			assertSame("ZoneID must be a number from 1-4, enter number 1,2,3,4 for zoneID",
					ExceptionHandler.getMessage());
		}
	}

}
